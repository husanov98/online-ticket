package uz.pdp.onlineticketforcinema.model.users;

//import static uz.mh.util.MyUtil.*;

import lombok.*;
import uz.pdp.onlineticketforcinema.model.template.AbsEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity(name = "permissions")
public class Permission extends AbsEntity {

    @Column(nullable = false)
    private String name;
}
