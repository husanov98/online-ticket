package uz.pdp.onlineticketforcinema.model.tickets;

import lombok.*;
import uz.pdp.onlineticketforcinema.model.enums.TicketStatus;
import uz.pdp.onlineticketforcinema.model.movies.Attachment;
import uz.pdp.onlineticketforcinema.model.sessions.MovieSession;
import uz.pdp.onlineticketforcinema.model.template.AbsEntity;
import uz.pdp.onlineticketforcinema.model.users.User;
import uz.pdp.onlineticketforcinema.model.watchingPlaces.Seat;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity(name = "tickets")
public class Ticket extends AbsEntity {

    @ManyToOne
    private MovieSession movieSession;

    @OneToOne
    private Seat seat;

    @OneToOne
    private Attachment qrCode;

    private Double price;

    @Enumerated(EnumType.STRING)
    private TicketStatus status=TicketStatus.NEW;

    @ManyToOne
    private User user;
}
