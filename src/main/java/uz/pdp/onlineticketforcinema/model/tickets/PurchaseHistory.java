package uz.pdp.onlineticketforcinema.model.tickets;

import lombok.*;
import uz.pdp.onlineticketforcinema.model.template.AbsEntity;
import uz.pdp.onlineticketforcinema.model.users.User;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity(name = "purchase_histories")
public class PurchaseHistory extends AbsEntity {

    @ManyToOne
    private User user;

    @ManyToOne
    private Ticket ticket;
}
