package uz.pdp.onlineticketforcinema.model.enums;

public enum CastType {
    CAST_ACTOR,
    CAST_DIRECTOR
}
