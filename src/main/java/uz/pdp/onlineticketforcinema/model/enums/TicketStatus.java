package uz.pdp.onlineticketforcinema.model.enums;

public enum TicketStatus {

    NEW,
    PURCHASED,
    REFUNDED
}
