package uz.pdp.onlineticketforcinema.model.enums;

public enum Gender {
    MALE,
    FEMALE
}
