package uz.pdp.onlineticketforcinema.model.movies;

import lombok.*;
import uz.pdp.onlineticketforcinema.model.template.AbsEntity;

import javax.persistence.Entity;

//import static uz.mh.util.MyUtil.*;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity(name = "attachments")
public class Attachment extends AbsEntity {

    private String fileName;

    private String contentType;

    private long size;

}
