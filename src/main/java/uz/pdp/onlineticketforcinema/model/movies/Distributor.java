package uz.pdp.onlineticketforcinema.model.movies;

import lombok.*;
import uz.pdp.onlineticketforcinema.model.template.AbsEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

//import static uz.mh.util.MyUtil.*;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity(name = "distributors")
public class Distributor extends AbsEntity {

    @Column(nullable = false)
    private String name;

    private String description;

}
