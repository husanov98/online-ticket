package uz.pdp.onlineticketforcinema.model.movies;

import lombok.*;
import uz.pdp.onlineticketforcinema.model.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

//import static uz.mh.util.MyUtil.*;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity(name = "attachment_contents")
public class AttachmentContent extends AbsEntity {
    private byte[] data;

    @OneToOne
    private Attachment attachment;

}
