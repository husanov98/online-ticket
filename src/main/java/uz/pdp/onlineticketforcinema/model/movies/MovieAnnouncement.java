package uz.pdp.onlineticketforcinema.model.movies;

import lombok.*;
import uz.pdp.onlineticketforcinema.model.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

//import static uz.mh.util.MyUtil.*;
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity(name = "movie_announcements")
public class MovieAnnouncement extends AbsEntity {

    @OneToOne
    private Movie movie;

    private Boolean isActive;
}
