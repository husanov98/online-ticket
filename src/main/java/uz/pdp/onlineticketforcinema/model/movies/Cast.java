package uz.pdp.onlineticketforcinema.model.movies;

//import static uz.mh.util.MyUtil.*;

import lombok.*;
import uz.pdp.onlineticketforcinema.model.enums.CastType;
import uz.pdp.onlineticketforcinema.model.template.AbsEntity;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity(name = "casts")
public class Cast extends AbsEntity {

    @Column(nullable = false)
    private String fullName;

    @OneToOne
    private Attachment photo;

    @Enumerated(EnumType.STRING)
    private CastType castType;

}
