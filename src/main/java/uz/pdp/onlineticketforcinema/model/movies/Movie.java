package uz.pdp.onlineticketforcinema.model.movies;

//import static uz.mh.util.MyUtil.*;

import lombok.*;
import uz.pdp.onlineticketforcinema.model.template.AbsEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity(name = "movies")
public class Movie extends AbsEntity {

    @Column(nullable = false,length = 50)
    private String title;

    private String description;

    private int durationInMin;

    @OneToOne
    private Attachment coverImage;

    @Column(nullable = false)
    private String trailerVideoUrl;

    private double minPrice;

    @Column(nullable = false)
    private Date releaseDate;

    private Double budget;

    @ManyToOne
    private Distributor distributor;

    @Column(nullable = false)
    private Double distributorShareInPercentage;

    @ManyToMany
    private List<Actor> actors;

    @ManyToMany
    private List<Genre> genres = new java.util.ArrayList<>();
}
