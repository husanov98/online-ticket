package uz.pdp.onlineticketforcinema.model.movies;

import lombok.*;
import uz.pdp.onlineticketforcinema.model.template.AbsEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

//import static uz.mh.util.MyUtil.*;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity(name = "actors")
public class Actor extends AbsEntity {

    @Column(nullable = false)
    private String fullName;

    @OneToOne
    private Attachment photo;
}
