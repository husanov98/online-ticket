package uz.pdp.onlineticketforcinema.model.sessions;

import lombok.*;

import javax.persistence.*;
import java.sql.Time;
import java.time.format.DateTimeFormatter;

//import static uz.mh.util.MyUtil.*;
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
public class SessionTime {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Time time;
    @ManyToOne
    private SessionDate sessionDate;
}
