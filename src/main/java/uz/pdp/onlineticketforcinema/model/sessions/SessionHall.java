package uz.pdp.onlineticketforcinema.model.sessions;

import lombok.*;

import javax.persistence.*;

//import static uz.mh.util.MyUtil.*;
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity(name = "session_halls")
public class SessionHall {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    private MovieSession session;
}
