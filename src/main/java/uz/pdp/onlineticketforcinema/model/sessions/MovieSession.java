package uz.pdp.onlineticketforcinema.model.sessions;

import lombok.*;
import uz.pdp.onlineticketforcinema.model.movies.MovieAnnouncement;
import uz.pdp.onlineticketforcinema.model.template.AbsEntity;
import uz.pdp.onlineticketforcinema.model.watchingPlaces.Hall;

import javax.persistence.*;

//import static uz.mh.util.MyUtil.*;
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity(name = "movie_sessions")
public class MovieSession extends AbsEntity {

    @ManyToOne
    private MovieAnnouncement movieAnnouncement;

    @ManyToOne
    private Hall hall;

    @ManyToOne
    private SessionDate startDate;

    @ManyToOne
    private SessionTime startTime;

    @ManyToOne
    private SessionTime endTime;
}
