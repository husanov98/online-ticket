package uz.pdp.onlineticketforcinema.model.watchingPlaces;

import lombok.*;
import uz.pdp.onlineticketforcinema.model.template.AbsEntity;

import javax.persistence.*;
import java.util.List;
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "price_categories")
public class PriceCategory extends AbsEntity {

    @Column(nullable = false,length = 50)
    private String name,color;

    @Column(nullable = false)
    private Double addFeeIntPercent;

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "priceCategory")
    private List<Seat> seats;

    public PriceCategory(String name, String color, Double addFeeIntPercent) {
        this.name = name;
        this.color = color;
        this.addFeeIntPercent = addFeeIntPercent;
    }
}