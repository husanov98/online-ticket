package uz.pdp.onlineticketforcinema.model.watchingPlaces;

import lombok.*;
import uz.pdp.onlineticketforcinema.model.template.AbsEntity;

import javax.persistence.*;
import java.util.List;

//import static uz.mh.util.MyUtil.*;
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity(name = "halls")
public class Hall extends AbsEntity {

    @Column(nullable = false,length = 50)
    private String name;

    @OneToMany(mappedBy = "hall", cascade = CascadeType.ALL)
    private List<Row> rows;

    private Double vipAddFeeInPercent;

    public Hall(String name) {
        this.name = name;
    }

    public Hall(String name, Double vipAddFeeInPercent) {
        this.name = name;
        this.vipAddFeeInPercent = vipAddFeeInPercent;
    }
}
