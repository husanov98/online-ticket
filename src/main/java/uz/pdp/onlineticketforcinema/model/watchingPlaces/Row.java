package uz.pdp.onlineticketforcinema.model.watchingPlaces;

import lombok.*;
import uz.pdp.onlineticketforcinema.model.template.AbsEntity;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity(name = "h_rows")
public class Row extends AbsEntity {

    @Column(nullable = false)
    private Integer number;

    @OneToMany(mappedBy = "row",cascade = CascadeType.ALL)
    private List<Seat> seats;

    @ManyToOne
    private Hall hall;

    public Row(Integer number, Hall hall) {
        this.number = number;
        this.hall = hall;
    }
}
