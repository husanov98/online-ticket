package uz.pdp.onlineticketforcinema.model.watchingPlaces;

import lombok.*;
import uz.pdp.onlineticketforcinema.model.template.AbsEntity;

import javax.persistence.*;
import java.util.List;

//import static uz.mh.util.MyUtil.*;
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity(name = "seats")
public class Seat extends AbsEntity {

    @Column(nullable = false)
    private Integer number;

    @ManyToOne
    private Row row;

    @ManyToOne
    private PriceCategory priceCategory;

    public Seat(Integer number, PriceCategory priceCategory) {
        this.number = number;
        this.priceCategory = priceCategory;
    }
}
