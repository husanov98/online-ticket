package uz.pdp.onlineticketforcinema.model.template;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;

import javax.persistence.*;
import java.sql.Timestamp;

//import static uz.mh.util.MyUtil.*;
@Data
@MappedSuperclass
public abstract class AbsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

//    @OrderBy
//    @CreationTimestamp
//    @Column(nullable = false,updatable = false)
//    private Timestamp createdAt;
//
//    @UpdateTimestamp
//    @Column(nullable = false)
//    private Timestamp updatedAt;
//
//    @CreatedBy
//    @Column(name = "created_by_id")
//    private Integer createdBy;
//
//    @LastModifiedBy
//    @Column(name = "updated_by_id")
//    private Integer updatedBy;
}
