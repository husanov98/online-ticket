package uz.pdp.onlineticketforcinema.service.interfaces;

import org.springframework.http.ResponseEntity;
import uz.pdp.onlineticketforcinema.dto.MovieSessionDto;

public interface MovieSessionService {
    ResponseEntity<?> getAllMovieSessions(int page, int size, String search);

    ResponseEntity<?> saveMovieSession(MovieSessionDto movieSessionDto);
}
