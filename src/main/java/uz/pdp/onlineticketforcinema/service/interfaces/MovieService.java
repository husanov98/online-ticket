package uz.pdp.onlineticketforcinema.service.interfaces;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.onlineticketforcinema.dto.MovieDto;

//import static uz.mh.util.MyUtil.*;

public interface MovieService {
    ResponseEntity getAllMovies(int page, int size, String search, String sort, boolean direction);

    ResponseEntity getMovieById(Integer id);

    ResponseEntity saveMovie(MovieDto movieDto, MultipartFile file);

    ResponseEntity deleteMovie(Integer id);
}
