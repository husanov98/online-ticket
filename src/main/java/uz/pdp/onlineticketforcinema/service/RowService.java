package uz.pdp.onlineticketforcinema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.onlineticketforcinema.model.watchingPlaces.Row;
import uz.pdp.onlineticketforcinema.payload.ApiResponse;
import uz.pdp.onlineticketforcinema.projection.RowProjection;
import uz.pdp.onlineticketforcinema.repository.RowRepository;

import java.util.List;

//import static uz.mh.util.MyUtil.*;
@Service
public class RowService {

    @Autowired
    RowRepository rowRepository;

    public ResponseEntity<?> getRows() {
        try {
            List<Row> rowList = rowRepository.findAll();
            return ResponseEntity.ok(
                    new ApiResponse("success", true, rowList)
            );

        } catch (Exception e) {
            return ResponseEntity.ok(
                    new ApiResponse("error", false, null));

        }
    }

    public ResponseEntity<?> getRowsByHallId(Integer hallId) {
        try {
            List<RowProjection> rowList = rowRepository.getRowsByHallId(hallId);
            return ResponseEntity.ok(
                    new ApiResponse("success", true, rowList));
        } catch (Exception e) {
            return ResponseEntity.ok(
                    new ApiResponse("error", false, null));
        }
    }
}
