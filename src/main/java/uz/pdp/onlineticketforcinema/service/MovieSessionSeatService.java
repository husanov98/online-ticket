package uz.pdp.onlineticketforcinema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.onlineticketforcinema.model.watchingPlaces.Seat;
import uz.pdp.onlineticketforcinema.payload.ApiResponse;
import uz.pdp.onlineticketforcinema.projection.SeatProjection;
import uz.pdp.onlineticketforcinema.repository.MovieSessionSeatRepository;

import java.util.List;

//import static uz.mh.util.MyUtil.*;
@Service
public class MovieSessionSeatService {

    @Autowired
    MovieSessionSeatRepository movieSessionSeatRepository;

    public ResponseEntity<?> getAllSeatsOfMovieSession(Integer movieSessionId) {
        try {
            List<SeatProjection> seatList = movieSessionSeatRepository.getAllSeatsOfMovieSession(movieSessionId);

            return ResponseEntity.ok(new ApiResponse("success", true, seatList));

        }catch (Exception e){
            return ResponseEntity.ok(new ApiResponse("error", false));
        }
    }
}
