package uz.pdp.onlineticketforcinema.service;

//import static uz.mh.util.MyUtil.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.onlineticketforcinema.dto.MovieDto;
import uz.pdp.onlineticketforcinema.model.movies.*;
import uz.pdp.onlineticketforcinema.payload.ApiResponse;
import uz.pdp.onlineticketforcinema.projection.CustomMovie;
import uz.pdp.onlineticketforcinema.projection.CustomMovieById;
import uz.pdp.onlineticketforcinema.repository.*;
import uz.pdp.onlineticketforcinema.service.interfaces.MovieService;

import java.util.List;
import java.util.Optional;

@Service
public class MovieServiceImpl implements MovieService {

    @Autowired
    MovieRepository movieRepository;

    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    DistributorRepository distributorRepository;

    @Autowired
    ActorRepository actorRepository;

    @Autowired
    GenreRepository genreRepository;

    @Override
    public ResponseEntity<?> getAllMovies(int page, int size, String search, String sort, boolean direction) {
        Pageable pageable = PageRequest.of(page - 1, size, direction ? Sort.Direction.ASC : Sort.Direction.DESC, sort);

        try {
            Page<CustomMovie> all = movieRepository.findAllByPage(pageable, search);

            return ResponseEntity.ok(new ApiResponse("success", true, all));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("error", false, null));
        }
    }

    @Override
    public ResponseEntity getMovieById(Integer id) {
        try {
            var movieById = movieRepository.findById(id);
//            CustomMovieById movie = movieById.get();
            Movie movie1 = movieById.get();
            var byId = movieRepository.getById(id);
            return ResponseEntity.ok(new ApiResponse("success", true,movie1));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("error", false, null));
        }
    }

    @Override
    public ResponseEntity saveMovie(MovieDto movieDto, MultipartFile file) {
        try {
            Movie movie = new Movie();
            if (movieDto.getId() != null) {
                movie.setId(movieDto.getId());
            }
            movie.setBudget(movieDto.getBudget());
            movie.setTitle(movieDto.getTitle());
            movie.setDescription(movieDto.getDescription());
            movie.setDurationInMin(movieDto.getDurationInMin());
            movie.setMinPrice(movieDto.getMinPrice());

            Attachment attachment = new Attachment();
            attachment.setContentType(file.getContentType());
            attachment.setSize(file.getSize());
            attachment.setFileName(file.getName());
            attachmentRepository.save(attachment);

            movie.setCoverImage(attachment);
            movie.setTrailerVideoUrl(movieDto.getTrailerVideoUrl());
            movie.setReleaseDate(movieDto.getReleaseDate());

            var distributorRepositoryById = distributorRepository.findById(movieDto.getDistributor());

            movie.setDistributor(distributorRepositoryById.get());

            movie.setDistributorShareInPercentage(movieDto.getDistributorShareInPercentage());

            var actors = actorRepository.findAllById(movieDto.getActors());
            movie.setActors(actors);

            var genres = genreRepository.findAllById(movieDto.getGenres());

            movie.setGenres(genres);

            movieRepository.save(movie);

            return ResponseEntity.ok(new ApiResponse("success", true, movie));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("error", false, null));
        }
    }

    @Override
    public ResponseEntity deleteMovie(Integer id) {
        try {
            movieRepository.deleteById(id);
            return ResponseEntity.ok(new ApiResponse("deleted", true));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("error", false, null));
        }
    }
}
