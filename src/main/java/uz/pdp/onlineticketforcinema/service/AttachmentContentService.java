package uz.pdp.onlineticketforcinema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.onlineticketforcinema.model.movies.Attachment;
import uz.pdp.onlineticketforcinema.model.movies.AttachmentContent;
import uz.pdp.onlineticketforcinema.payload.ApiResponse;
import uz.pdp.onlineticketforcinema.repository.AttachmentContentRepository;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

//import static uz.mh.util.MyUtil.*;
@Service
public class AttachmentContentService {

    @Autowired
    AttachmentContentRepository contentRepository;

    public ResponseEntity addAttachmentCon(MultipartHttpServletRequest request) {
        try {
            Iterator<String> fileNames = request.getFileNames();
            MultipartFile file1 = request.getFile(fileNames.next());
            Attachment attachment = new Attachment();
            attachment.setFileName(file1.getOriginalFilename());
            attachment.setContentType(file1.getContentType());
            attachment.setSize(file1.getSize());
            AttachmentContent attachmentContent = new AttachmentContent();
            attachmentContent.setData(file1.getBytes());
            attachmentContent.setAttachment(attachment);

            contentRepository.save(attachmentContent);
            return ResponseEntity.ok(new ApiResponse("added", true));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("error", false));
        }
    }

    public ResponseEntity getAllAttachContents() {
        try {
            List<AttachmentContent> contentList = contentRepository.findAll();
            return ResponseEntity.ok(new ApiResponse("success", true, contentList));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("error", false));
        }
    }

    public ResponseEntity updateAttachContent(AttachmentContent attachmentContent, Integer id) {
        var attachmentContentById = contentRepository.findById(id);
        if (!attachmentContentById.isPresent()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("not found", false));
        }
        try {
            AttachmentContent updatingAttachCon = attachmentContentById.get();
            updatingAttachCon.setAttachment(attachmentContent.getAttachment());
            updatingAttachCon.setData(attachmentContent.getData());
            contentRepository.save(updatingAttachCon);
            return ResponseEntity.ok(new ApiResponse("Successfully updated", true, updatingAttachCon));

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("error", false));
        }
    }

    public ResponseEntity getAttachConById(Integer id) {
        var attachConById = contentRepository.findById(id);
        if (!attachConById.isPresent()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("not found", false));
        }
        try {
            var attachConById1 = contentRepository.findById(id);
            AttachmentContent attachmentContent = attachConById1.get();
            return ResponseEntity.ok(new ApiResponse("Successfully updated", true, attachmentContent));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("error", false));
        }
    }

    public ResponseEntity deleteAttachmentContent(Integer id) {
        var attachmentContent = contentRepository.findById(id);
        if (!attachmentContent.isPresent()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("not found", false));
        }
        try {
            contentRepository.deleteById(id);
            return ResponseEntity.ok(new ApiResponse("deleted", true));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("error", false));
        }
    }
}
