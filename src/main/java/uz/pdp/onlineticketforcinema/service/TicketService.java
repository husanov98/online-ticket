package uz.pdp.onlineticketforcinema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.onlineticketforcinema.model.enums.TicketStatus;
import uz.pdp.onlineticketforcinema.model.sessions.MovieSession;
import uz.pdp.onlineticketforcinema.model.tickets.PurchaseHistory;
import uz.pdp.onlineticketforcinema.model.tickets.Ticket;
import uz.pdp.onlineticketforcinema.model.users.User;
import uz.pdp.onlineticketforcinema.model.watchingPlaces.Seat;
import uz.pdp.onlineticketforcinema.payload.ApiResponse;
import uz.pdp.onlineticketforcinema.projection.TicketProjection;
import uz.pdp.onlineticketforcinema.repository.*;

import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;

@Service
public class TicketService {

    @Autowired
    TicketRepository ticketRepository;

    @Autowired
    MovieSessionSeatRepository movieSessionSeatRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    MovieSessionRepository movieSessionRepository;

    @Autowired
    PurchaseHistoryRepository purchaseHistoryRepository;


    public ResponseEntity<?> getTicketInfo(Integer movieSessionId,Integer seatId,Integer userId) {
        try {
            TicketProjection ticketInfo =  ticketRepository.getTicketInfo(movieSessionId,seatId,userId);
//            var ticketProjection = ticketInfo.get();
            return ResponseEntity.ok(new ApiResponse("success", true,ticketInfo));

        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("error", false, null));
        }
    }

    public HttpEntity addTicketToCart(Integer movieSessionId, Integer seatId, Integer userId) {
      /*  try {
            Ticket ticket = ticketRepository.addTicketToCart(movieSessionId,seatId,userId);
            return ResponseEntity.ok(new ApiResponse("success", true));

        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("error", false, null));

        }*/

        Ticket ticket=new Ticket();
        Optional<MovieSession> byIdMovieSession = movieSessionRepository.findById(movieSessionId);

        if (!byIdMovieSession.isPresent()) {
            return ResponseEntity.ok(new ApiResponse("Wrong", false,null));
        }

        Optional<Seat> byIdSeatOptional = movieSessionSeatRepository.findById(seatId);
        if (!byIdSeatOptional.isPresent()) {
            return ResponseEntity.ok(new ApiResponse("Wrong", false,null));
        }

        Optional<User> userById = userRepository.findById(userId);
        if (!userById.isPresent()) {
            return ResponseEntity.ok(new ApiResponse("Wrong", false,null));
        }

        Seat seat = byIdSeatOptional.get();

        MovieSession movieSession = byIdMovieSession.get();

        User user = userById.get();

        Double price = ticketRepository.getTicketPrice(movieSessionId,seatId);

        ticket.setMovieSession(movieSession);
        ticket.setSeat(seat);
        ticket.setUser(user);
        ticket.setPrice(price);
        ticket.setStatus(TicketStatus.NEW);
        var save = ticketRepository.save(ticket);
        scheduleDeleteTicket(save);
        return ResponseEntity.ok(new ApiResponse("success", true));
    }

    public void scheduleDeleteTicket(Ticket ticket){
        try {
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    Ticket ticketFromDb = ticketRepository.findById(ticket.getId()).get();
                    if (ticketFromDb.getStatus().equals(TicketStatus.NEW)) {
                        ticketRepository.delete(ticketFromDb);
                    }
                }
            };
            Timer timer = new Timer();
            long delayTime = 15000;
            timer.schedule(timerTask,delayTime);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public ResponseEntity<?> purchaseTicket(Integer ticketId) {
        Ticket ticket = ticketRepository.findById(ticketId).get();

        purchaseHistoryRepository.save(new PurchaseHistory(ticket.getUser(),ticket));

        ticket.setStatus(TicketStatus.PURCHASED);
        ticket.setUser(null);
        ticketRepository.save(ticket);

        return ResponseEntity.ok(new ApiResponse("Ticket successfully purchased",true));
    }
}
