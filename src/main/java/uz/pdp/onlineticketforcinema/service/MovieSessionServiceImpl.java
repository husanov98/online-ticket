package uz.pdp.onlineticketforcinema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.onlineticketforcinema.dto.MovieSessionDto;
import uz.pdp.onlineticketforcinema.model.sessions.MovieSession;
import uz.pdp.onlineticketforcinema.payload.ApiResponse;
import uz.pdp.onlineticketforcinema.projection.MovieSessionProjection;
import uz.pdp.onlineticketforcinema.repository.MovieSessionRepository;
import uz.pdp.onlineticketforcinema.service.interfaces.MovieSessionService;


@Service
public class MovieSessionServiceImpl implements MovieSessionService {

    @Autowired
    MovieSessionRepository movieSessionRepository;

    @Override
    public ResponseEntity<?> getAllMovieSessions(int page, int size, String search) {
        Pageable pageable = PageRequest.of(page - 1, size);
        Page<MovieSessionProjection> all = movieSessionRepository.findAllSessionByPage(pageable, search);
        return ResponseEntity.ok(new ApiResponse("success", true, all));
    }

    @Override
    public ResponseEntity<?> saveMovieSession(MovieSessionDto movieSessionDto) {
        try {
// TODO: 3/29/2022
            MovieSession movieSession = new MovieSession();
            return ResponseEntity.ok(new ApiResponse("success", true));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("error", false, null));
        }
    }
}
