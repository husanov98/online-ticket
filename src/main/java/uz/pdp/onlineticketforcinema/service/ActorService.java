package uz.pdp.onlineticketforcinema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.onlineticketforcinema.model.movies.Actor;
import uz.pdp.onlineticketforcinema.model.movies.Attachment;
import uz.pdp.onlineticketforcinema.model.movies.AttachmentContent;
import uz.pdp.onlineticketforcinema.payload.ApiResponse;
import uz.pdp.onlineticketforcinema.repository.ActorRepository;
import uz.pdp.onlineticketforcinema.repository.AttachmentContentRepository;
import uz.pdp.onlineticketforcinema.repository.AttachmentRepository;

import java.util.List;
import java.util.Optional;

//import static uz.mh.util.MyUtil.*;
@Service
public class ActorService {

    @Autowired
    ActorRepository actorRepository;

    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    AttachmentContentRepository contentRepository;

    public ResponseEntity addActor(Actor actor, MultipartFile file) {
        try {
            Attachment attachment = new Attachment();
            attachment.setFileName(file.getOriginalFilename());
            attachment.setContentType(file.getContentType());
            attachment.setSize(file.getSize());
            attachmentRepository.save(attachment);

            AttachmentContent attachmentContent = new AttachmentContent();
            attachmentContent.setAttachment(attachment);
            attachmentContent.setData(file.getBytes());
            contentRepository.save(attachmentContent);

            actor.setPhoto(attachment);
            var savedActor = actorRepository.save(actor);
            return ResponseEntity.ok(new ApiResponse("Actor saved", true, savedActor));
        } catch (Exception e) {
            return ResponseEntity.ok(new ApiResponse("error", false));

        }
    }

    public ResponseEntity getAllActors() {
        try {
            List<Actor> actors = actorRepository.findAll();
            return ResponseEntity.ok(new ApiResponse("Success", true, actors));
        } catch (Exception e) {
            return ResponseEntity.ok(new ApiResponse("error", false));
        }
    }

    public ResponseEntity updateActor(Actor actor, MultipartFile file, Integer id) {
        var actorById = actorRepository.findById(id);
        if (!actorById.isPresent()) {
            return ResponseEntity.ok(new ApiResponse("Actor is not found", false));
        }
        try {
            Actor updatingActor = actorById.get();
            updatingActor.setFullName(actor.getFullName());
            if (file.isEmpty()) {
                var savingActor = actorRepository.save(updatingActor);
                return ResponseEntity.ok(new ApiResponse("Actor is saved", true, savingActor));
            }
            Attachment imageOfActor = updatingActor.getPhoto();
            imageOfActor.setFileName(file.getOriginalFilename());
            imageOfActor.setSize(file.getSize());
            imageOfActor.setContentType(file.getContentType());
            var savedAttachment = attachmentRepository.save(imageOfActor);

            var byAttachmentId = contentRepository.findByAttachmentId(savedAttachment.getId());
            AttachmentContent attachmentContent = byAttachmentId.get();
            attachmentContent.setAttachment(savedAttachment);
            attachmentContent.setData(file.getBytes());
            contentRepository.save(attachmentContent);
            updatingActor.setPhoto(savedAttachment);
            var savedActor = actorRepository.save(updatingActor);
            return ResponseEntity.ok(new ApiResponse("Actor is saved", true, savedActor));

        } catch (Exception e) {
            return ResponseEntity.ok(new ApiResponse("Error", false));
        }
    }

    public ResponseEntity getActorById(Integer id) {

        var actorById = actorRepository.findById(id);
        if (!actorById.isPresent()) {
            return ResponseEntity.ok(new ApiResponse("Not found", false));
        }
        try {
            return ResponseEntity.ok(new ApiResponse("Success", true, actorById));

        } catch (Exception e) {
            return ResponseEntity.ok(new ApiResponse("Error", false));
        }
    }

    public ResponseEntity deleteActor(Integer id) {
        var actorById = actorRepository.findById(id);
        if (!actorById.isPresent()) {
            return ResponseEntity.ok(new ApiResponse("Not found", false));
        }
        try {
            Actor actor = actorById.get();
            Attachment attachment = actor.getPhoto();
            if (attachment == null){
                actorRepository.deleteById(actor.getId());
                return ResponseEntity.ok(new ApiResponse("Actor is deleted from list", true,actor));
            }
            var byAttachmentId = contentRepository.findByAttachmentId(attachment.getId());
            AttachmentContent attachmentContent = byAttachmentId.get();
            contentRepository.deleteById(attachmentContent.getId());
            attachmentRepository.deleteById(attachment.getId());
            actorRepository.deleteById(actor.getId());
            return ResponseEntity.ok(new ApiResponse("Actor is deleted from list", true));
        } catch (Exception e) {
            return ResponseEntity.ok(new ApiResponse("Error", false));
        }
    }
}
