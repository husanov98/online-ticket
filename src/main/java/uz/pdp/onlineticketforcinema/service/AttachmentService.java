package uz.pdp.onlineticketforcinema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.onlineticketforcinema.model.movies.Attachment;
import uz.pdp.onlineticketforcinema.model.movies.AttachmentContent;
import uz.pdp.onlineticketforcinema.payload.ApiResponse;
import uz.pdp.onlineticketforcinema.repository.AttachmentContentRepository;
import uz.pdp.onlineticketforcinema.repository.AttachmentRepository;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

//import static uz.mh.util.MyUtil.*;
@Service
public class AttachmentService {

    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    AttachmentContentRepository attachmentContentRepository;

    public ResponseEntity fileUpload(MultipartHttpServletRequest file) {

        try {
            Iterator<String> fileNames = file.getFileNames();
            MultipartFile file1 = file.getFile(fileNames.next());
            Attachment attachment = new Attachment();
            attachment.setFileName(file1.getOriginalFilename());
            attachment.setContentType(file1.getContentType());
            attachment.setSize(file1.getSize());
            AttachmentContent attachmentContent = new AttachmentContent();
            attachmentContent.setData(file1.getBytes());
            attachmentContent.setAttachment(attachment);
            attachmentRepository.save(attachment);

            attachmentContentRepository.save(attachmentContent);
            return ResponseEntity.ok(new ApiResponse("added", true));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("error", false));
        }
    }

    public ResponseEntity getAllAttachments() {
        try {
            List<Attachment> attachmentList = attachmentRepository.findAll();
            return ResponseEntity.ok(new ApiResponse("success", true, attachmentList));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("error", false));
        }
    }

    public ResponseEntity updateAttachment(Attachment attachment, Integer id) {
        Optional<Attachment> attachmentById = attachmentRepository.findById(id);
        if (!attachmentById.isPresent()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("not found", false));
        }
        try {
            Attachment updatingAttachment = attachmentById.get();
            updatingAttachment.setFileName(attachment.getFileName());
            updatingAttachment.setContentType(attachment.getContentType());
            updatingAttachment.setSize(attachment.getSize());
            attachmentRepository.save(updatingAttachment);
            return ResponseEntity.ok(new ApiResponse("Successfully updated", true, updatingAttachment));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("error", false));
        }
    }

    public ResponseEntity deleteAttachment(Integer id) {
        Optional<Attachment> attachment = attachmentRepository.findById(id);
        if (!attachment.isPresent()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("not found", false));
        }
        try {
            attachmentRepository.deleteById(id);
            return ResponseEntity.ok(new ApiResponse("deleted", true));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("error", false));

        }
    }

    public ResponseEntity getAttachmentById(Integer id) {
        Optional<Attachment> attachment = attachmentRepository.findById(id);
        if (!attachment.isPresent()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("not found", false));
        }
        try {
            var attachmentById = attachmentRepository.findById(id);
            Attachment attachment1 = attachmentById.get();
            return ResponseEntity.ok(new ApiResponse("success", true, attachment1));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("error", false));
        }
    }
}
