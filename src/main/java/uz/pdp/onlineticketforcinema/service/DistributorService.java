package uz.pdp.onlineticketforcinema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.onlineticketforcinema.model.movies.Distributor;
import uz.pdp.onlineticketforcinema.payload.ApiResponse;
import uz.pdp.onlineticketforcinema.repository.DistributorRepository;

import java.util.List;
import java.util.Optional;

//import static uz.mh.util.MyUtil.*;
@Service
public class DistributorService {

    @Autowired
    DistributorRepository distributorRepository;

    public ResponseEntity getDistributors() {
        try {
            List<Distributor> distributorList = distributorRepository.findAll();

            return ResponseEntity.ok(new ApiResponse("success", true, distributorList));
        } catch (Exception e) {
            return ResponseEntity.ok(new ApiResponse("error", false, null));
        }
    }

    public ResponseEntity addDistributor(Distributor distributor) {
        try {
            distributorRepository.save(distributor);
            return ResponseEntity.ok(new ApiResponse("added", true, distributor));
        } catch (Exception e) {
            return ResponseEntity.ok(new ApiResponse("error", false, null));
        }

    }

    public ResponseEntity deleteById(Integer id) {
        Optional<Distributor> distributorById = distributorRepository.findById(id);
        if (!distributorById.isPresent()){
            return ResponseEntity.ok(new ApiResponse("not found",false,null));
        }
        try {
            distributorRepository.deleteById(id);
            return ResponseEntity.ok(new ApiResponse("deleted", true, null));
        } catch (Exception e) {
            return ResponseEntity.ok(new ApiResponse("error", false, null));
        }
    }

    public ResponseEntity updateDistributor(Distributor distributor, Integer id) {
        Optional<Distributor> distributorById = distributorRepository.findById(id);
        if (!distributorById.isPresent()) {
            return ResponseEntity.ok(new ApiResponse("not found", false, null));
        }
            try {
                Distributor editingDistributor = distributorById.get();
                editingDistributor.setName(distributor.getName());
                editingDistributor.setDescription(distributor.getDescription());
                distributorRepository.save(editingDistributor);
                return ResponseEntity.ok(new ApiResponse("Successfully edited", true, editingDistributor));
            } catch (Exception e) {
                return ResponseEntity.ok(new ApiResponse("error", false, null));
            }
        }

    public ResponseEntity getDistributorById(Integer id) {
        try {
            var distributorById = distributorRepository.findById(id);
            Distributor distributor = distributorById.get();
            return ResponseEntity.ok(new ApiResponse("success", true, distributor));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("error", false));

        }
    }
}

