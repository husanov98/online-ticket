package uz.pdp.onlineticketforcinema.dto;


import lombok.*;
import uz.pdp.onlineticketforcinema.model.movies.*;

import javax.persistence.Column;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.util.Date;
import java.util.List;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class MovieDto {

    private Integer id;

    private String title;

    private String description;

    private int durationInMin;

    private Attachment coverImage;

    private String trailerVideoUrl;

    private double minPrice;

    private Date releaseDate;

    private Double budget;

    private Integer distributor;

    private Double distributorShareInPercentage;

    private List<Integer> actors;

    private List<Integer> genres = new java.util.ArrayList<>();

}
