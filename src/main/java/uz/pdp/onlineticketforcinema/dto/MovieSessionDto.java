package uz.pdp.onlineticketforcinema.dto;


import lombok.*;
import uz.pdp.onlineticketforcinema.model.movies.MovieAnnouncement;
import uz.pdp.onlineticketforcinema.model.sessions.SessionDate;
import uz.pdp.onlineticketforcinema.model.sessions.SessionTime;
import uz.pdp.onlineticketforcinema.model.watchingPlaces.Hall;

import javax.persistence.ManyToOne;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class MovieSessionDto {

    private Integer movieAnnouncement;


    private Integer hall;


    private Integer startDate;


    private Integer startTime;


    private Integer endTime;
}
