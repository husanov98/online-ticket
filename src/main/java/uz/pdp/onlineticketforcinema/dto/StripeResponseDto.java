package uz.pdp.onlineticketforcinema.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class StripeResponseDto {
    private String sessionId;
}
