package uz.pdp.onlineticketforcinema.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class TicketDto {

    private Integer movieSessionId;
    private Integer seatId;

    private double price;
    private String movieTitle;
}
