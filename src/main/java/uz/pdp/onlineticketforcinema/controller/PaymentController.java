package uz.pdp.onlineticketforcinema.controller;

import com.stripe.exception.StripeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.onlineticketforcinema.dto.TicketDto;
import uz.pdp.onlineticketforcinema.service.PaymentService;

import java.util.List;

@RestController
public class PaymentController {

    @Autowired
    PaymentService paymentService;

    @PostMapping("/create-checkout-session")
    public ResponseEntity<?> createCheckoutSession(@RequestBody List<TicketDto> ticketDtoList) throws StripeException{
        return paymentService.createStripeSession(ticketDtoList);
    }
}
