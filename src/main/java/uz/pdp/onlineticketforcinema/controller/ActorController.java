package uz.pdp.onlineticketforcinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.onlineticketforcinema.model.movies.Actor;
import uz.pdp.onlineticketforcinema.service.ActorService;

//import static uz.mh.util.MyUtil.*;
@RestController
@RequestMapping("/api/actor")
public class ActorController {

    @Autowired
    ActorService actorService;

    @PostMapping
    public ResponseEntity addActor(@RequestPart("json-actor") Actor actor, @RequestPart("file") MultipartFile file) {
        return actorService.addActor(actor, file);
    }

    @GetMapping
    public ResponseEntity getAllActors() {
        return actorService.getAllActors();
    }

    @PutMapping
    public ResponseEntity updateActor(@RequestPart Actor actor, @RequestPart MultipartFile file, @RequestParam Integer id) {
        return actorService.updateActor(actor, file, id);
    }

    @GetMapping("/{id}")
    public ResponseEntity getActorById(@PathVariable Integer id) {
        return actorService.getActorById(id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteActor(@PathVariable Integer id){
        return actorService.deleteActor(id);
    }
}
