package uz.pdp.onlineticketforcinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.onlineticketforcinema.model.movies.AttachmentContent;
import uz.pdp.onlineticketforcinema.service.AttachmentContentService;

//import static uz.mh.util.MyUtil.*;
@RestController
@RequestMapping("/api/attachmentContent")
public class AttachmentContentController {

    @Autowired
    AttachmentContentService contentService;

    @PostMapping
    public ResponseEntity addAttachmentCon(MultipartHttpServletRequest request) {
        return contentService.addAttachmentCon(request);
    }

    @GetMapping
    public ResponseEntity getAllAttachContents() {
        return contentService.getAllAttachContents();
    }

    @PutMapping
    public ResponseEntity updateAttachContent(@RequestBody AttachmentContent attachmentContent, @PathVariable Integer id) {
        return contentService.updateAttachContent(attachmentContent,id);
    }

    @GetMapping("/{id}")
    public ResponseEntity getAttachConById(@PathVariable Integer id){
        return contentService.getAttachConById(id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteAttachmentContent(@PathVariable Integer id){
        return contentService.deleteAttachmentContent(id);
    }
}
