package uz.pdp.onlineticketforcinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.onlineticketforcinema.service.RowService;

//import static uz.mh.util.MyUtil.*;
@RestController
@RequestMapping("/api/row")
public class RowController {

    @Autowired
    RowService rowService;

    @GetMapping
    public ResponseEntity<?> getRows(){
        return rowService.getRows();
    }

    @GetMapping("/{hallaid}")
    public ResponseEntity<?> getRowsByHallId(@PathVariable Integer hallId){
        return rowService.getRowsByHallId(hallId);
    }
}
