package uz.pdp.onlineticketforcinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.onlineticketforcinema.model.movies.Attachment;
import uz.pdp.onlineticketforcinema.service.AttachmentService;

//import static uz.mh.util.MyUtil.*;
@RestController
@RequestMapping("/api/attachment")
public class AttachmentController {

    @Autowired
    AttachmentService attachmentService;

    @PostMapping
    public ResponseEntity fileUpload(MultipartHttpServletRequest file){
        return attachmentService.fileUpload(file);
    }

    @GetMapping
    public ResponseEntity getAllAttachments(){
        return attachmentService.getAllAttachments();
    }

    @PutMapping("/{id}")
    public ResponseEntity updateAttachment(@RequestBody Attachment attachment, @PathVariable Integer id){
        return attachmentService.updateAttachment(attachment,id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteAttachment(@PathVariable Integer id){
        return attachmentService.deleteAttachment(id);
    }

    @GetMapping("/{id}")
    public ResponseEntity getAttachmentById(@PathVariable Integer id){
        return attachmentService.getAttachmentById(id);
    }
}
