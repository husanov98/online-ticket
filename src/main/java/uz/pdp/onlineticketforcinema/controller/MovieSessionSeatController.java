package uz.pdp.onlineticketforcinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.onlineticketforcinema.service.MovieSessionSeatService;

@RestController
@RequestMapping("/api/seat")
public class MovieSessionSeatController {

    @Autowired
    MovieSessionSeatService movieSessionSeatService;

    @GetMapping("/{id}")
    public ResponseEntity<?> getAllSeatsByMovieSessionId(@PathVariable(name = "id") Integer movieSessionId) {
        return movieSessionSeatService.getAllSeatsOfMovieSession(movieSessionId);
    }
}
