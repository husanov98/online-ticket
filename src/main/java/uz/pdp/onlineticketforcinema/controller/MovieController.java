package uz.pdp.onlineticketforcinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.onlineticketforcinema.dto.MovieDto;
import uz.pdp.onlineticketforcinema.service.interfaces.MovieService;
import uz.pdp.onlineticketforcinema.util.Constant;

//import static uz.mh.util.MyUtil.*;
@RestController
@RequestMapping("/api/movie")
public class MovieController {

    @Autowired
    MovieService movieService;

    @GetMapping
    public ResponseEntity getAllMovies(@RequestParam(name = "size",defaultValue = Constant.DEFAULT_VALUE_SIZE) int size,
                                       @RequestParam(name = "page",defaultValue = "1") int page,
                                       @RequestParam(name = "search",defaultValue = "") String search,
                                       @RequestParam(name = "sort",defaultValue = "title") String sort)
    {
        return movieService.getAllMovies(page,size,search,sort,true);
    }

    @PostMapping
    public ResponseEntity<?> saveMovie(@RequestPart("movie") MovieDto movieDto, @RequestPart("file") MultipartFile file){
        return movieService.saveMovie(movieDto,file);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteMovie(@PathVariable Integer id){
        return movieService.deleteMovie(id);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getMovieById(@PathVariable Integer id){
        return movieService.getMovieById(id);
    }

}
