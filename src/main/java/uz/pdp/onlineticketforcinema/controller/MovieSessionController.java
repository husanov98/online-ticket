package uz.pdp.onlineticketforcinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.pdp.onlineticketforcinema.dto.MovieSessionDto;
import uz.pdp.onlineticketforcinema.service.interfaces.MovieSessionService;
import uz.pdp.onlineticketforcinema.util.Constant;

//import static uz.mh.util.MyUtil.*;
@RestController
@RequestMapping("/api/movie-session")
public class MovieSessionController {

    @Autowired
    MovieSessionService movieSessionService;

    @GetMapping
    public ResponseEntity<?> getAllMovieSessions(@RequestParam(name = "size", defaultValue = Constant.DEFAULT_VALUE_SIZE) int size,
                                                 @RequestParam(name = "page", defaultValue = "1") int page,
                                                 @RequestParam(name = "search", defaultValue = "") String search) {
        return movieSessionService.getAllMovieSessions(page, size, search);
    }

    @PostMapping
    public ResponseEntity<?> saveMovieSession(@RequestPart(name = "movieSession")MovieSessionDto movieSessionDto){
        return movieSessionService.saveMovieSession(movieSessionDto);
    }
}
