package uz.pdp.onlineticketforcinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.onlineticketforcinema.model.movies.Distributor;
import uz.pdp.onlineticketforcinema.payload.ApiResponse;
import uz.pdp.onlineticketforcinema.service.DistributorService;

//import static uz.mh.util.MyUtil.*;
@RestController
@RequestMapping("/api/distributor")
public class DistributorController {

    @Autowired
    DistributorService distributorService;

    @GetMapping
    public ResponseEntity getAllDistributors(){
        return distributorService.getDistributors();
    }


    @PostMapping()
    public ResponseEntity addDistributor(@RequestBody Distributor distributor){
        return distributorService.addDistributor(distributor);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable Integer id){
        return distributorService.deleteById(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity updateDistributor(@RequestBody Distributor distributor,@PathVariable Integer id){
        return distributorService.updateDistributor(distributor,id);
    }

    @GetMapping("/{id}")
    public ResponseEntity getDistributorById(@PathVariable Integer id){
        return distributorService.getDistributorById(id);
    }
}
