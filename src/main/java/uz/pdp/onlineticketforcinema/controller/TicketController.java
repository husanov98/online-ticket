package uz.pdp.onlineticketforcinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.onlineticketforcinema.service.TicketService;

@RestController
@RequestMapping("/api/ticket")
public class TicketController {

    @Autowired
    TicketService ticketService;

    @GetMapping("/{id}/{id1}/{userId}")
    public ResponseEntity<?> getTicketInfo(@PathVariable(name = "id") Integer movieSessionId,@PathVariable(name = "id1") Integer seatId,@PathVariable(name = "userId") Integer userId){
        return ticketService.getTicketInfo(movieSessionId,seatId,userId);
    }

    @PostMapping("/addToCart/{id}/{seatId}/{userId}")
    public HttpEntity<?> addTicketToCart(@PathVariable(name = "id") Integer movieSessionId, @PathVariable(name = "seatId") Integer seatId, @PathVariable(name = "userId") Integer userId){
        return ticketService.addTicketToCart(movieSessionId,seatId,userId);
    }

    @GetMapping("/purchase/{ticketId}")
    public ResponseEntity<?> purchaseTicket(@PathVariable Integer ticketId){
        return ticketService.purchaseTicket(ticketId);
    }
}
