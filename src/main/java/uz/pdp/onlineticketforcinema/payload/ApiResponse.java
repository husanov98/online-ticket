package uz.pdp.onlineticketforcinema.payload;

import lombok.*;

import javax.persistence.Entity;

//import static uz.mh.util.MyUtil.*;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString

public class ApiResponse {

    private String message;

    private boolean isSuccess;

    private Object data;

    public ApiResponse(String message, boolean isSuccess) {
        this.message = message;
        this.isSuccess = isSuccess;
    }
}
