package uz.pdp.onlineticketforcinema.projection;

public interface TicketProjection {

    Integer getSeatNumber();

    Integer getRowNumber();

    String getHallName();

    Double getTicketPrice();
}
