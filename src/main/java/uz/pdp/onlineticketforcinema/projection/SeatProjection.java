package uz.pdp.onlineticketforcinema.projection;

public interface SeatProjection {

    Integer getId();

    Integer getNumber();

    Integer getPriceCategoryId();

    Integer getRowNumber();

    String getHallName();
}
