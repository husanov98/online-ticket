package uz.pdp.onlineticketforcinema.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.onlineticketforcinema.model.movies.Movie;

import java.time.LocalDate;
import java.util.List;

@Projection(types = Movie.class)
public interface CustomMovieById {

    Integer getId();

    String getTitle();

    Integer getCoverImgId();

    LocalDate getReleaseDate();

    String getDescription();

    Integer getDurationInMin();

    @Value("#{@genreRepository.getGenresByMovieId(target.id)}")
    List<GenreProjection> getGenres();
}
