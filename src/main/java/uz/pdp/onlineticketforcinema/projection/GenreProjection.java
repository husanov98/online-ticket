package uz.pdp.onlineticketforcinema.projection;

//import static uz.mh.util.MyUtil.*;

public interface GenreProjection {

    Integer getId();

    String getName();
}
