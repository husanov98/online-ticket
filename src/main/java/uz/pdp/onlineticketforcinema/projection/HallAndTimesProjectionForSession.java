package uz.pdp.onlineticketforcinema.projection;

import org.springframework.beans.factory.annotation.Value;

import java.util.List;

public interface HallAndTimesProjectionForSession {

    Integer getId();

    String getName();

    @Value("#{@sessionTimeRepository.getTimesByHallIdAndAnnouncementIdAndStartDateId(target.id, target.movieAnnouncementId, target.startDateId)}")
    List<SessionTimeProjection> getTimes();

}
