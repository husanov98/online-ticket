package uz.pdp.onlineticketforcinema.projection;

public interface RowProjection {

    Integer getId();

    Integer getNumber();

    String getHallName();
}
