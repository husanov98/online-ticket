package uz.pdp.onlineticketforcinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.onlineticketforcinema.model.sessions.SessionTime;
import uz.pdp.onlineticketforcinema.projection.SessionTimeProjection;

import java.util.List;

public interface SessionTimeRepository extends JpaRepository<SessionTime, Integer> {

    @Query(value = "select distinct st.id, ms.id as sessionId\n" +
            "from session_time st\n" +
            "         join movie_sessions ms on st.id = ms.start_time_id\n" +
            "where ms.hall_id = :hallId\n" +
            "  and movie_announcement_id = :movieAnnouncementId\n" +
            "  and ms.start_date_id = :startDateId", nativeQuery = true)
    List<SessionTimeProjection> getTimesByHallIdAndAnnouncementIdAndStartDateId(Integer hallId, Integer movieAnnouncementId, Integer startDateId);
}
