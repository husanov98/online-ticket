package uz.pdp.onlineticketforcinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.onlineticketforcinema.model.movies.Attachment;
@Repository
public interface AttachmentRepository extends JpaRepository<Attachment,Integer> {
}
