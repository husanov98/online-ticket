package uz.pdp.onlineticketforcinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.onlineticketforcinema.model.watchingPlaces.Hall;
import uz.pdp.onlineticketforcinema.projection.HallAndTimesProjectionForSession;

import java.util.List;

public interface HallRepository extends JpaRepository<Hall,Integer> {
    @Query(nativeQuery = true,value = "select distinct h.id, h.name, rh.start_date_id as startDateId, " +
            "movie_announcement_id as movieAnnouncementId\n" +
            "from halls h\n" +
            "         join movie_sessions rh on h.id = rh.hall_id\n" +
            "where rh.start_date_id = :startDateId\n" +
            "  and movie_announcement_id = :movieAnnouncementId")
    List<HallAndTimesProjectionForSession> getHallsAndTimesByMovieAnnouncementIdAndStartDateId(Integer movieAnnouncementId, Integer startDateId);
}
