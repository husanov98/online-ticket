package uz.pdp.onlineticketforcinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.onlineticketforcinema.model.tickets.PurchaseHistory;

public interface PurchaseHistoryRepository extends JpaRepository<PurchaseHistory,Integer> {
}
