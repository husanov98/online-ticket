package uz.pdp.onlineticketforcinema.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.onlineticketforcinema.model.tickets.Ticket;
import uz.pdp.onlineticketforcinema.projection.TicketProjection;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Integer> {

    @Query(value = "select s.number as seatNumber,\n" +
            "       r.number as rowNumber,\n" +
            "       h.name   as hallName,\n" +
            "       t.price  as ticketPrice\n" +
            "\n" +
            "from tickets t\n" +
            "         join movie_sessions ms on t.movie_session_id = ms.id\n" +
            "         join seats s on t.seat_id = s.id\n" +
            "         join users u on t.user_id=u.id\n" +
            "         join h_rows r on s.h_row_id = r.id\n" +
            "         join halls h on ms.hall_id = h.id\n" +
            "where ms.id =:movieSessionId\n" +
            "and s.id =:seatId\n" +
            "and u.id =:userId",nativeQuery = true)
    TicketProjection getTicketInfo(Integer movieSessionId,Integer seatId,Integer userId);

//    Ticket addTicketToCart(Integer movieSessionId, Integer seatId, Integer userId);

    @Query(value = "select (m.min_price +\n" +
            "        ((m.min_price + (price_categories.add_fee_int_percent/100 * m.min_price)) * h.vip_add_fee_in_percent/100)) as sum1\n" +
            "from price_categories\n" +
            "         join seats s on price_categories.id = s.price_category_id\n" +
            "         join tickets t on s.id = t.seat_id\n" +
            "         join movie_sessions ms on ms.id = t.movie_session_id\n" +
            "         join halls h on h.id = ms.hall_id\n" +
            "         join movies m on m.id = ms.movie_id\n" +
            "where ms.id =:movieSessionId\n" +
            "  and s.id =:seatId",nativeQuery = true)
    Double getTicketPrice(Integer movieSessionId, Integer seatId);
}
