package uz.pdp.onlineticketforcinema.repository;

//import static uz.mh.util.MyUtil.*;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import uz.pdp.onlineticketforcinema.model.movies.Distributor;
@Repository
@EnableJpaRepositories
public interface DistributorRepository extends JpaRepository<Distributor,Integer> {
}
