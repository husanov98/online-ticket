package uz.pdp.onlineticketforcinema.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;
import uz.pdp.onlineticketforcinema.model.movies.Movie;
import uz.pdp.onlineticketforcinema.projection.CustomMovie;
import uz.pdp.onlineticketforcinema.projection.CustomMovieById;

import java.util.Optional;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer> {
    @Query(value = "select m.id,\n" +
            "       title,\n" +
            "       release_date as releaseDate,\n" +
            "       a.id   as coverImgId\n" +
            "from movies m\n" +
            "         join attachments a on a.id = m.cover_image_id\n" +
            "where lower(title) like lower(concat('%', :search, '%'))", nativeQuery = true)
    Page<CustomMovie> findAllByPage(Pageable pageable, String search);

    @Query(nativeQuery = true,value = "select id,\n" +
            "       title,\n" +
            "       cover_image_id  as coverImgId,\n" +
            "       release_date    as releaseDate,\n" +
            "       duration_in_min as durationInMin,\n" +
            "       description\n" +
            "from movies m\n" +
            "where m.id = :id")
    Optional<CustomMovieById> getMovieById(Integer id);
}
