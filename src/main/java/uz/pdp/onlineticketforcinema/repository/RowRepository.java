package uz.pdp.onlineticketforcinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.onlineticketforcinema.model.watchingPlaces.Row;
import uz.pdp.onlineticketforcinema.projection.RowProjection;

import java.util.List;

public interface RowRepository extends JpaRepository<Row,Integer> {
    @Query(nativeQuery = true,value = "select * from rows r\n" +
            "join halls h on r.hall_id = h.id\n" +
            "where h.id = :hallId")
    List<RowProjection> getRowsByHallId(Integer hallId);
}
