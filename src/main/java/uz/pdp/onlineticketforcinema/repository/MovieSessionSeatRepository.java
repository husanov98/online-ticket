package uz.pdp.onlineticketforcinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.onlineticketforcinema.model.watchingPlaces.Seat;
import uz.pdp.onlineticketforcinema.projection.SeatProjection;
import uz.pdp.onlineticketforcinema.service.MovieSessionSeatService;

import java.util.List;

@Repository
public interface MovieSessionSeatRepository extends JpaRepository<Seat, Integer> {
    @Query(value = "select seats.id,\n" +
            "       seats.number            as number,\n" +
            "       seats.price_category_id as priceCategoryId,\n" +
            "       r.number                as rowNumber,\n" +
            "       h.name                  as hallName\n" +
            "from seats\n" +
            "         join h_rows r on r.id = seats.h_row_id\n" +
            "         join halls h on h.id = r.hall_id\n" +
            "         join movie_sessions ms on h.id = ms.hall_id\n" +
            "where seats.id not in (\n" +
            "    select t.seat_id\n" +
            "    from tickets t\n" +
            "             join movie_sessions ms on ms.id = t.movie_session_id\n" +
            "    where t.status <> 'REFUNDED'\n" +
            "      and ms.id = :movieSessionId\n" +
            ")\n" +
            "  and ms.id = :movieSessionId", nativeQuery = true)
    List<SeatProjection> getAllSeatsOfMovieSession(Integer movieSessionId);
}
