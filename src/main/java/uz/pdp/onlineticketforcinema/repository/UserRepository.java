package uz.pdp.onlineticketforcinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.onlineticketforcinema.model.movies.Actor;
import uz.pdp.onlineticketforcinema.model.users.User;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {
}
