package uz.pdp.onlineticketforcinema.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.onlineticketforcinema.model.sessions.MovieSession;
import uz.pdp.onlineticketforcinema.projection.MovieSessionProjection;

public interface MovieSessionRepository extends JpaRepository<MovieSession, Integer> {
    @Query(nativeQuery = true, value = "select distinct ma.id            as movieAnnouncementId,\n" +
            "                ma.movie_id      as movieId,\n" +
            "                m.title          as movieTitle,\n" +
            "                sd.id            as startDateId,\n" +
            "                sd.date          as startDate,\n" +
            "                m.cover_image_id as movieCoverImgId\n" +
            "from movie_sessions ms\n" +
            "         join movie_announcements ma on ms.movie_id = ma.movie_id\n" +
            "         join movies m on ma.movie_id = m.id\n" +
            "         join session_dates sd on sd.id = ms.start_date_id\n" +
            "where lower(m.title) like lower(concat('%', :search, '%'))\n" +
            "  and sd.date >= cast(now() as date) order by sd.date")
     Page<MovieSessionProjection> findAllSessionByPage(Pageable pageable, String search);
}
