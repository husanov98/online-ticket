package uz.pdp.onlineticketforcinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.onlineticketforcinema.model.movies.Actor;
@Repository
public interface ActorRepository extends JpaRepository<Actor,Integer> {
}
