package uz.pdp.onlineticketforcinema;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineTicketForCinemaApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnlineTicketForCinemaApplication.class, args);
    }

}
